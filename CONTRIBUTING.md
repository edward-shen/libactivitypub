### Code style
Please follow [Google's C++ styling guide][googlestyle] with the following
exceptions:
 - Use camelCase for function names.

[googlestyle]: https://google.github.io/styleguide/cppguide.html
