#include <vector>
#include "activitystreams/link.h"

#ifndef LIBACTIVITYPUB_ACTIVITYSTREAMS_OBJECT
#define LIBACTIVITYPUB_ACTIVITYSTREAMS_OBJECT

namespace activitystreams {

class Object {
  public:
    void getAttachment();
    void setAttachment(Object o);
    void setAttachment(Link l);
  private:
    void attachment;


};

}

#endif
