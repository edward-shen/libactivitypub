#include <optional>
#include <string>
#include <vector>

#ifndef LIBACTIVITYPUB_ACTIVITYSTREAMS_LINK
#define LIBACTIVITYPUB_ACTIVITYSTREAMS_LINK

namespace activitystreams {

class Link {
  public:
    std::optional<std::string> getHref();
    void setHref(std::string href);
    void removeHref();

    std::vector<std::string> getRel();
    void addRel(std::string toAdd);
    bool removeRelElement(std::string toRemove);
    void clearRel();




    std::optional<int> getHeight();
    void setHeight(int h);
    void removeHeight();

    std::optional<int> getWidth();
    void setWidth(int w);
    void removeWidth();

  private:
    std::optional<std::string> href;
    std::vector<std::string> rel;


    std::optional<int> height;
    std::optional<int> width;

};

}

#endif
