#include <regex>

namespace utils {

// URI regex conforms to RFC 3986 Appendix B
// https://tools.ietf.org/html/rfc3986#appendix-B
std::regex uri_regex (
  R"(^(([^:\/?#]+):)?(//([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?)",
  std::regex::extended
);

std::smatch uri_match_result;

bool isUri(std::string uri) {
  return std::regex_match(uri, uri_match_result, uri_regex);
}

}
