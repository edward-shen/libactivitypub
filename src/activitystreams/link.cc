#include <stdexcept>
#include <algorithm>

#include "../utils.h"
#include "../../include/activitystreams/link.h"

namespace activitystreams {

std::optional<std::string> Link::getHref() {
  return href;
}

void Link::setHref(std::string str) {
  if (utils::isUri(str)) {
    href.emplace(str);
  } else {
    throw std::invalid_argument("Attempted to set href to non-URI!");
  }
}

void Link::removeHref() {
  href.reset();
}


std::vector<std::string> Link::getRel() {
  return rel;
}


void Link::addRel(std::string toAdd) {
  // Check if string conforms to link relation requirements as defined in
  // https://www.w3.org/TR/activitystreams-vocabulary/#dfn-rel
  if (toAdd.find("\x20") != std::string::npos
    || toAdd.find("\x0A") != std::string::npos
    || toAdd.find("\x0C") != std::string::npos
    || toAdd.find("\x2C") != std::string::npos) {
      throw std::invalid_argument("Attempted to add malformed link rel!");
  }

  rel.push_back(toAdd);
}

bool Link::removeRelElement(std::string toRemove) {
  const unsigned int size = rel.size();
  rel.erase(std::remove(rel.begin(), rel.end(), toRemove), rel.end());
  return rel.size() != size;
}

void Link::clearRel() {
  rel.clear();
}

std::optional<int> Link::getHeight() {
  return height;
}

void Link::setHeight(int h) {
  if (h < 0) {
    throw std::invalid_argument("Attempted to set height to negative value!");
  }

  height.emplace(h);
}

void Link::removeHeight() {
  height.reset();
}

std::optional<int> Link::getWidth() {
  return width;
}

void Link::setWidth(int w) {
  if (w < 0) {
    throw std::invalid_argument("Attempted to set width to negative value!");
  }

  width.emplace(w);
}

void Link::removeWidth() {
  width.reset();
}


}
